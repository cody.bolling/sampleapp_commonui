﻿using System;
using CommonUI.BaseClasses;
using Xamarin.Forms;

namespace CommonUI
{
    public class MainPageViewModel : ViewModelBase
    {
        public Command Command { get; private set; }

        private int _Num;
        public int Num
        {
            get { return _Num; }
            set { SetProperty(ref _Num, value); }
        }

        private string _Text;
        public string Text
        {
            get { return _Text; }
            set { SetProperty(ref _Text, value); }
        }

        private TimeSpan _Time;
        public TimeSpan Time
        {
            get { return _Time; }
            set { SetProperty(ref _Time, value); }
        }

        public MainPageViewModel()
        {
            Num = 10;
            Text = "";
            Command = new Command<int>((x) => TestCommand(x));
        }

        private void TestCommand(int n)
        {
            var t = Time;
            string a = Text;
        }
    }
}
